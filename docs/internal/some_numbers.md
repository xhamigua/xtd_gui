| [Home](home.md) | [News](news.md) | [Gallery](gallery.md) | [Examples](examples.md) | [Downloads](downloads.md) | [Documentation](documentation.md) | [Support](support.md) | [Sources](https://github.com/gammasoft71/xtd) | [Project](https://sourceforge.net/projects/xtdpro/) | [Gammasoft](gammasoft.md) | [Website](https://gammasoft71.github.io/xtd) |

# xtd in numbers (last update 2022-12-23)

## Key dates

* start project : 2022-12-23
* First release (beta) : 2021-10-10

## Team

* developers : 2
* translators (*) : 0
* documenters (*) : 0
* testers (*) : 0

(*) without developers

## Files and lines

### src

* Files : 1623
* Lines : 258330
  * 117635 code lines (46%)
  * 114432 comment lines (44%)
  *  26263 blank lines (10%)

### tests

* Files : 324
* Lines : 49595
  * 41906 code lines (85%)
  *   715 comment lines (#%)
  *  6974 blank lines (14%)

### examples

* Examples : 529
* Files : 1773
* Lines : 54250
  * 38430 code lines (71%)
  *  3959 comment lines (7%)
  * 11861 blank lines (12%)

### tools

* Files : 94
* Lines : 8422
  * 7361 code lines (87%)
  *  121 comment lines (1%)
  *  940 blank lines (12%)
  
## Unit tests

* xtd.core.unit_tests : 
   * test_case : 284
   * tests : 9374
   
* xtd.drawing.unit_tests : 
   * test_case : 3
   * tests : 319
   
* xtd.forms.unit_tests : 
   * test_case : 28
   * tests : 386
   
* xtd.forms.unit_tests : 
   * tests : 282

