#include <xtd/xtd>

using namespace xtd::forms;

auto main()->int {
  application::run(form::create("Build output directory"));
}
