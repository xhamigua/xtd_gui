#include <xtd/xtd>
#include <iostream>

using namespace std;
using namespace xtd;

namespace startup_example {
  class program {
  public:
    static auto main(const vector<ustring>& args) {
      cout << ustring::format("args = {}", ustring::join(", ", args)) << endl;
      environment::exit_code(42);
    }
  };
}

startup_(startup_example::program);

// This code produces the following output if one two "three four" five six are entered on command line:
//
// args = one, two, three four, five, six
