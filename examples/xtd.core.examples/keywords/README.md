# Keywords examples

[This folder](.) contains keywords examples used by [Reference Guide](https://gammasoft71.github.io/xtd/reference_guides/latest/) and more.

* [block_scope](block_scope/README.md) shows how to use [xtd::block_scope](https://gammasoft71.github.io/xtd/reference_guides/latest/block__scope_8h.html) method.
* [current_stack_frame](current_stack_frame/README.md) shows how to use [xtd::diagnostics::stack_frame](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1diagnostics_1_1stack__frame.html) method.
* [csf](csf/README.md) shows how to use [xtd::diagnostics::stack_frame](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1diagnostics_1_1stack__frame.html) method.
* [interface](interface/README.md) shows how to use [xtd::interface](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1interface.html) method.
* [interface2](interface2/README.md) shows how to use [xtd::interface](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1interface.html) method.
* [startup](startup/README.md) shows how to use [xtd::startup](https://gammasoft71.github.io/xtd/reference_guides/latest/startup_8h.html) method.
* [static](static/README.md) shows how to use [static_](https://gammasoft71.github.io/xtd/reference_guides/latest/group__keywords.html#ga28796443ec37b938df7072c79595e3f6) keyword.
* [static_object](static_object/README.md) shows how to use [xtd::static_object](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1static__object.html) keyword.
* [static_object2](static_object2/README.md) shows how to use [xtd::static_object](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1static__object.html) class.
* [using](using/README.md) shows how to use [xtd::using](https://gammasoft71.github.io/xtd/reference_guides/latest/using_8h.html) method.
