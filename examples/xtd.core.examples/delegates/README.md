# Delegates examples

[This folder](.) contains delegates examples used by [Reference Guide](https://gammasoft71.github.io/xtd/reference_guides/latest/) and more.

* [action](action/README.md) shows how to use [xtd::action](https://gammasoft71.github.io/xtd/reference_guides/latest/group__xtd__core.html#ga53c721aac682a7ccf19ee17d92280e31) alias.
* [action1](action1/README.md) shows how to simplify code by instantiating the action delegate instead of explicitly defining a new delegate and assigning a named method to it.
* [action2](action2/README.md) shows how to simplify code by instantiating the action delegate instead of explicitly defining a new delegate and assigning a named method to it.
* [action3](action3/README.md) shows how to simplify code by instantiating the action delegate instead of explicitly defining a new delegate and assigning a named method to it.
* [delegate](delegate/README.md) shows how to use [xtd::delegate](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1delegate_3_01result__t_07_08_4.html) class.
* [delegate_lambda](delegate_lambda/README.md) shows how to use [xtd::delegate](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1delegate_3_01result__t_07_08_4.html) class.
* [delegate_member_method](delegate_member_method/README.md) shows how to use [xtd::delegate](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1delegate_3_01result__t_07_08_4.html) class.
* [delegate_multicast](delegate_multicast/README.md) shows how to use [xtd::delegate](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1delegate_3_01result__t_07_08_4.html) class.
* [event](event/README.md) shows how to use [xtd::event](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1event.html) class.
