# xtd.core Threading examples

[This folder](.) contains network examples used by [Reference Guide](https://gammasoft71.github.io/xtd/reference_guides/latest/) and more.

* [interlocked](interlocked/README.md) shows hows how to use [xtd::network::interlocked](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1threading_1_1interlocked.html) class.
* [interlocked_decrement](interlocked_decrement/README.md) shows hows how to use [xtd::threading::interlocked::decrement](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1threading_1_1interlocked.html#a4f4545f0c5952db7df8ff6fc4aa41067) and [xtd::threading::interlocked::increment](https://gammasoft71.github.io/xtd/reference_guides/latest/classxtd_1_1threading_1_1interlocked.html#acf60d0d23279ede3b23ddee39265aadd) methods.
