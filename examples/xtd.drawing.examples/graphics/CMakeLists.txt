cmake_minimum_required(VERSION 3.3)

project(graphics)
find_package(xtd REQUIRED)

add_projects(
  graphics
)
