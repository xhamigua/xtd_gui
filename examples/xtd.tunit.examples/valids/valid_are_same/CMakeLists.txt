cmake_minimum_required(VERSION 3.3)

project(valid_are_same)
find_package(xtd REQUIRED)
add_sources(README.md src/valid_are_same.cpp)
target_type(TEST_APPLICATION)
