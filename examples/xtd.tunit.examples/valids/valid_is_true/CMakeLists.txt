cmake_minimum_required(VERSION 3.3)

project(valid_is_true)
find_package(xtd REQUIRED)
add_sources(README.md src/valid_is_true.cpp)
target_type(TEST_APPLICATION)
